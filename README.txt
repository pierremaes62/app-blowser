# Image mobile dette

## Démarrage de l'application

### Démarrage du serveur

La version de python utilisé pour ces dev est la 3.7

Avant toute chose, il faut commencer par initialiser les BDD du serveur.
Il faut donc lancer la commande:

```sh
python manage.py migrate
```

Ensuite, il est possible de démarrer le serveur python avec la commande :

```sh
python manage.py runserver
```

### Démarrage de l'application android

L'application se lance via AndroidStudio.

La version du SDK d'android utilisé est la Android 9.0 (version d'api 28)
Après avoir effectué un Graddle install, il devrait être possible d'émuler l'application.


## Les fonctionnalités ajoutés
### Possibilité d'envoyer une image vers le serveur

Cette nouvelle fonctionnalité se situe dans la galerie de photo, accessible via l'icone ronde en bas a droite de l'écran.
On peut choisir ensuite la fonction à evoyer en appuyant sur l'icone d'envoi représenté par un avion en papier.

Une nouvelle fenetre s'ouvre, avec une preview de la photo selmectionné, ainsi que des champs non obligatoires "mail" et "commentaire"

Il est ensuite possible d'envoyer la photo vers le serveur. Celle ci sera stocké en BDD.

### Possibilité de "Upvoter" ou "Downvoter" les résultats envoyés par le serveur

Lors de l'envoi d'une photo vers le serveur pour effectuer une recherche d'image similaire, le serveur renvoie les trois images qui se rapprochent le plus de celle envoyé.
Celle-ci étaient déjà visible à la base via une popup contenant les dites photos.

Désormais, sous chaque résultats, un bouton "V" (Upvote) et "X" (Downvote) sont visibles.
Lors d'une pression sur l'un des deux boutons, on envoie le vote directement vers le serveur, et les deux boutons se désactivent, sans possibilité de revoter pour la meme photo.

Les votes sont stockés dans une nouvelle BDD coté serveur, nommé "image_rate", et contenant le type du vote (rate), l'url de la photo "résultat" envoyé par le serveur (matched_image) et l'id en BDD de la photo que l'on avait envoyé au serveur (sent_image_id, clé étrangère de image_file.id)



