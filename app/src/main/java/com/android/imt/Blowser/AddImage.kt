package com.android.imt.Blowser

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.Response
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputLayout
import java.io.File


private const val ARG_PARAM1 = "param1"

/**
 * A simple [Fragment] subclass.
 * Use the [AddImage.newInstance] factory method to
 * create an instance of this fragment.
 */

// PARTIE 1 RATTRAPAGE

class AddImage() : Fragment() {
    private lateinit var image_path: String
    // CHANGER LIEN PAR RAPPORT AU LIEN NGROCK COURANT
    private val postURL: String = "https://c2c7261acc03.ngrok.io/submit"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            this.image_path = it.get(ARG_PARAM1) as String
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var matrix = Matrix()
        matrix.postRotate(90F)
        var imageFile = File(this.image_path)
        // Get mail and comment

        // Show image in fragment
        val imageBytes = BitmapFactory.decodeFile(imageFile.path)
        val rotatedImageBitmap = Bitmap.createBitmap(imageBytes, 0, 0, imageBytes.width, imageBytes.height, matrix, true)
        view.findViewById<ImageView>(R.id.image_preview).setImageBitmap(rotatedImageBitmap)
        // Handle back button press
        view.findViewById<Button>(R.id.submit_button).setOnClickListener {
            val text = "Envoi de l'image en cours..."
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(context?.applicationContext, text, duration)
            toast.show()

            val imageData = imageFile.readBytes()

            val request = object : VolleyFileUploadRequest(
                Method.POST,
                postURL,
                Response.Listener {
                    val text = "Image envoyé!"
                    val duration = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(context?.applicationContext, text, duration)
                    toast.show()
                    getFragmentManager()?.popBackStack()
                },
                Response.ErrorListener {
                    println("error is: $it")


                    val text = "Une erreur est survenue"
                    val duration = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(context?.applicationContext, text, duration)
                    toast.show()
                }
            ){
                override fun getByteData(): MutableMap<String, Any> {
                    var params = HashMap<String, Any>()
                    val commentInput = view.findViewById(R.id.comment_textinput) as TextInputLayout
                    val comment = commentInput.editText?.text.toString()
                    val mailInput = view.findViewById(R.id.mail_textinput) as EditText
                    val mail = mailInput.text.toString()
                    params["file"] = FileDataPart("image", imageData!!, "jpeg")
                    params["mail"] = mail
                    params["comment"] = comment
                    return params
                }
            }
            println(request)
            Volley.newRequestQueue(context).add(request)
        }
    }
}