/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.imt.Blowser

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.os.Bundle
import android.view.Gravity
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.NavHostFragment
import com.android.imt.Blowser.fragments.PhotoPreview
import com.android.imt.Blowser.fragments.PhotoPreviewDirections
import com.android.imt.Blowser.utils.FLAGS_FULLSCREEN
import com.android.volley.Response
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_photo_preview.*
import org.json.JSONObject
import java.io.File
import java.net.URL


const val KEY_EVENT_ACTION = "key_event_action"
const val KEY_EVENT_EXTRA = "key_event_extra"
private const val IMMERSIVE_FLAG_TIMEOUT = 500L

/**
 * Main entry point into our app. This app follows the single-activity pattern, and all
 * functionality is implemented in the form of fragments.
 */
class MainActivity : AppCompatActivity(), PhotoPreview.OnFragmentCallbacks {
    private lateinit var container: FrameLayout

    // CHANGER LIENS PAR RAPPORT AU LIEN NGROCK COURANT
    private val postURL: String = "https://c2c7261acc03.ngrok.io/searches"
    private val postRateURL: String = "https://c2c7261acc03.ngrok.io/rate"
    private val imageURL: String = "https://c2c7261acc03.ngrok.io"

    private lateinit var lastImage : Bitmap


    override fun setImage(image : File) {
        println(R.id.imageView)
        val imageViewTmp  = findViewById<ImageView>(R.id.imageView)
        val matrix = Matrix()

        matrix.postRotate(90F)

        val scaledBitmap = BitmapFactory.decodeFile(image.path)

        val rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
        lastImage = rotatedBitmap
        imageViewTmp.setImageBitmap(rotatedBitmap)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        container = findViewById(R.id.fragment_container)
    }

    override fun onResume() {
        super.onResume()
        // Before setting full screen flags, we must wait a bit to let UI settle; otherwise, we may
        // be trying to set app to immersive mode before it's ready and the flags do not stick
        container.postDelayed({
            container.systemUiVisibility = FLAGS_FULLSCREEN
        }, IMMERSIVE_FLAG_TIMEOUT)
    }

    /** When key down event is triggered, relay it via local broadcast so fragments can handle it */
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_VOLUME_DOWN -> {
                val intent = Intent(KEY_EVENT_ACTION).apply { putExtra(KEY_EVENT_EXTRA, keyCode) }
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
                true
            }
            else -> super.onKeyDown(keyCode, event)
        }
    }

    companion object {

        /** Use external media if it is available, our app's file directory otherwise */
        fun getOutputDirectory(context: Context): File {
            val appContext = context.applicationContext
            val mediaDir = context.externalMediaDirs.firstOrNull()?.let {
                File(it, appContext.resources.getString(R.string.app_name)).apply { mkdirs() } }
            return if (mediaDir != null && mediaDir.exists())
                mediaDir else appContext.filesDir
        }
    }

    override fun sendImage(imageFile : File) {
        val text = "Envoi de l'image en cours..."
        val duration = Toast.LENGTH_SHORT

        val toast = Toast.makeText(applicationContext, text, duration)
        toast.show()

        val imageData = imageFile.readBytes()
        val request = object : VolleyFileUploadRequest(
                Method.POST,
                postURL,
                Response.Listener {
                    println("response is: $it")
                    val jsonRep = JSONObject(String(it.data))
                    println("$jsonRep")
                    val window = PopupWindow(this)
                    val view = layoutInflater.inflate(R.layout.popup_result, null)
                    window.contentView = view
                    window.showAtLocation(imageView, Gravity.TOP, 0 ,0)

                    window.contentView.findViewById<Button>(R.id.quit_popup).setOnClickListener {
                        window.dismiss()
                        val action = PhotoPreviewDirections.actionPhotoPreviewToCameraFragment()
                        val navHostFragment = supportFragmentManager
                                .findFragmentById(R.id.fragment_container) as NavHostFragment
                        val navController = navHostFragment.navController
                        navController.navigate(action)
                    }

                    val imgResults = jsonRep.getJSONArray("match")
                    val urls = ArrayList<String>()
                    for (i in 0 until imgResults.length()) {
                        val url = imgResults.getJSONObject(i).get("path").toString()
                        urls.add(url)
                    }
                    val text = "Reception des images similaires"
                    val duration = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.show()
                    window.contentView.findViewById<ImageView>(R.id.baseImage).setImageBitmap(lastImage)

                    for (imageUrl in 0 until urls.size){
                        Thread {
                            val url = URL("$imageURL"+ "${urls[imageUrl]}")
                            val bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream())

                            // CONTENU DU THREAD MODIFIÉ POUR LA PARTIE 2 DU RATTRAPAGE
                            runOnUiThread {
                                val stringIdImageView = "imageResult${imageUrl+1}"
                                val stringIdUpVote = "upVote${imageUrl+1}"
                                val stringIdDownVote = "downVote${imageUrl+1}"
                                val idImageView = resources.getIdentifier(stringIdImageView, "id", packageName)
                                val idUpVoteView = resources.getIdentifier(stringIdUpVote, "id", packageName)
                                val idDownVoteView = resources.getIdentifier(stringIdDownVote, "id", packageName)
                                val idMainImage = jsonRep.getInt("id")
                                window.contentView.findViewById<ImageView>(idImageView).setImageBitmap(bmp)
                                window.contentView.findViewById<ImageButton>(idUpVoteView).setOnClickListener {
                                    rateImage(idMainImage,urls[imageUrl],1)
                                    unactiveButtons(view, idUpVoteView, idDownVoteView)
                                }
                                window.contentView.findViewById<ImageButton>(idDownVoteView).setOnClickListener {
                                    rateImage(idMainImage,urls[imageUrl],0)
                                    unactiveButtons(view, idDownVoteView, idUpVoteView)
                                }
                            }
                        }.start()
                    }
                },
                Response.ErrorListener {
                    println("error is: $it")

                    val cancelBtn = findViewById<ImageButton>(R.id.cancel_action)
                    val sendBtn = findViewById<ImageButton>(R.id.send_action)
                    cancelBtn.isClickable = true
                    cancelBtn.isEnabled = true
                    sendBtn.isEnabled = true
                    sendBtn.isClickable = true

                    val text = "Une erreur est survenue"
                    val duration = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.show()

                }
        ) {
            override fun getByteData(): MutableMap<String, FileDataPart> {
                var params = HashMap<String, FileDataPart>()
                params["file"] = FileDataPart("image", imageData!!, "jpeg")
                return params
            }
        }
        Volley.newRequestQueue(this).add(request)
    }

    // FONCTION VOTE PARTIE 2 RATTRAPAGE
    override fun rateImage(sent_image_id: Int, matched_image_url: String, rate: Int) {
        val request = object : VolleyRateUploadRequest(
                Method.POST,
                postRateURL,
                Response.Listener {
                    val text = "Vote bien prix en compte!"
                    val duration = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.show()


                },
                Response.ErrorListener {
                    println("error is: $it")


                    val text = "Une erreur est survenue"
                    val duration = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.show()
                }
        ){
            override fun getByteData(): MutableMap<String, Any> {
                var params = HashMap<String, Any>()
                params["sent_image"] = sent_image_id
                params["matched_image"] = matched_image_url
                params["rate"] = rate
                return params
            }
        }
        Volley.newRequestQueue(this).add(request)
    }

    override fun unactiveButtons(view: View, up_button_id: Int, down_button_id: Int) {
        val upVoteButton = view.findViewById<ImageButton>(up_button_id)
        val downVoteButton = view.findViewById<ImageButton>(down_button_id)
        upVoteButton.isClickable = false
        upVoteButton.isEnabled = false
        downVoteButton.isClickable = false
        downVoteButton.isEnabled = false
        downVoteButton.setColorFilter(Color.argb(150, 255, 255, 255))
    }
}
