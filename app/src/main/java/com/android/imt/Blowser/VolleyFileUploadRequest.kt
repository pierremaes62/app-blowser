package com.android.imt.Blowser

import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import java.io.*
import kotlin.jvm.Throws
import kotlin.math.min


open class VolleyFileUploadRequest(
        method: Int,
        url: String,
        listener: Response.Listener<NetworkResponse>,
        errorListener: Response.ErrorListener) : Request<NetworkResponse>(method, url, errorListener) {
    private var responseListener: Response.Listener<NetworkResponse>? = null
    init {
        this.responseListener = listener
    }

    private var headers: Map<String, String>? = null
    private val divider: String = "--"
    private val ending = "\r\n"
    private val boundary = "imageRequest${System.currentTimeMillis()}"


    override fun getHeaders(): MutableMap<String, String> =
            when(headers) {
                null -> super.getHeaders()
                else -> headers!!.toMutableMap()
            }

    override fun getBodyContentType() = "multipart/form-data;boundary=$boundary"


    @Throws(AuthFailureError::class)
    override fun getBody(): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        val dataOutputStream = DataOutputStream(byteArrayOutputStream)
        try {
            if (params != null && params.isNotEmpty()) {
                processParams(dataOutputStream, params, paramsEncoding)
            }
            val data = getByteData()
            if (data != null && data.isNotEmpty()) {
                processData(dataOutputStream, data)
            }
            dataOutputStream.writeBytes(divider + boundary + divider + ending)
            return byteArrayOutputStream.toByteArray()

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return super.getBody()
    }

    @Throws(AuthFailureError::class)
    open fun getByteData(): Map<String, Any>? {
        return null
    }

    override fun parseNetworkResponse(response: NetworkResponse): Response<NetworkResponse> {
        return try {
            Response.success(response, HttpHeaderParser.parseCacheHeaders(response))
        } catch (e: Exception) {
            Response.error(ParseError(e))
        }
    }

    override fun deliverResponse(response: NetworkResponse) {
        responseListener?.onResponse(response)
    }

    override fun deliverError(error: VolleyError) {
        errorListener?.onErrorResponse(error)
    }

    @Throws(IOException::class)
    private fun processParams(dataOutputStream: DataOutputStream, params: Map<String, String>, encoding: String) {
        try {
            params.forEach {
                dataOutputStream.writeBytes(divider + boundary + ending)
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"${it.key}\"$ending")
                dataOutputStream.writeBytes(ending)
                dataOutputStream.writeBytes(it.value + ending)
            }
        } catch (e: UnsupportedEncodingException) {
            throw RuntimeException("Unsupported encoding not supported: $encoding with error: ${e.message}", e)
        }
    }

    // FONCTION MODIFIÉ PARTIE 1 RATTRAPAGE
    @Throws(IOException::class)
    private fun processData(dataOutputStream: DataOutputStream, data: Map<String, Any>) {
        data.forEach {
            var dataValue = it.value
            dataOutputStream.writeBytes("$divider$boundary$ending")
            if (dataValue.javaClass.name.equals("com.android.imt.Blowser.FileDataPart")) {
                dataValue = dataValue as FileDataPart
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"${it.key}\"; filename=\"${dataValue.fileName}\"$ending")
                if (dataValue.type != null && dataValue.type.trim().isNotEmpty()) {
                    dataOutputStream.writeBytes("Content-Type: ${dataValue.type}$ending")
                }
                dataOutputStream.writeBytes(ending)
                val fileInputStream = ByteArrayInputStream(dataValue.data)
                var bytesAvailable = fileInputStream.available()
                val maxBufferSize = 1024 * 1024
                var bufferSize = min(bytesAvailable, maxBufferSize)
                val buffer = ByteArray(bufferSize)
                var bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                while (bytesRead > 0) {
                    dataOutputStream.write(buffer, 0, bufferSize)
                    bytesAvailable = fileInputStream.available()
                    bufferSize = min(bytesAvailable, maxBufferSize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                }
                dataOutputStream.writeBytes(ending)
            }
            if (dataValue.javaClass.name.equals("java.lang.String")) {
                dataValue = dataValue.toString()
                val dataString = dataValue as String
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"${it.key}\"$ending")

                dataOutputStream.writeBytes(ending)
                val dataInputStream = ByteArrayInputStream(dataString.toByteArray())
                var bytesAvailable = dataInputStream.available()
                val maxBufferSize = 1024 * 1024
                var bufferSize = min(bytesAvailable, maxBufferSize)
                val buffer = ByteArray(bufferSize)
                var bytesRead = dataInputStream.read(buffer, 0, bufferSize)
                while (bytesRead > 0) {
                    dataOutputStream.write(buffer, 0, bufferSize)
                    bytesAvailable = dataInputStream.available()
                    bufferSize = min(bytesAvailable, maxBufferSize)
                    bytesRead = dataInputStream.read(buffer, 0, bufferSize)
                }
                dataOutputStream.writeBytes(ending)
            }
        }
    }
}

class FileDataPart(var fileName: String?, var data: ByteArray, var type: String)