package com.android.imt.Blowser.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.android.imt.Blowser.R
import kotlinx.android.synthetic.main.fragment_photo_preview.*
import java.io.File
import java.util.*


class PhotoPreview : Fragment() {

    private val args: PhotoPreviewArgs by navArgs()

    // Liste des photos dans le dossier
    private lateinit var mediaList: MutableList<File>

    //Dernière photo prise
    private lateinit var lastPicture: File

    private var imageData: ByteArray? = null
    private val postURL: String = "https://ptsv2.com/t/54odo-1576291398/post"

    var callbacks: OnFragmentCallbacks? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callbacks = activity as OnFragmentCallbacks
    }

    interface OnFragmentCallbacks{
        fun setImage(image: File)
        fun sendImage(image: File)
        fun rateImage(sent_image_id: Int, matched_image_url: String, rate: Int)
        fun unactiveButtons(view: View, up_button_id: Int, down_button_id: Int)
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        // Get root directory of media from navigation arguments
        val rootDirectory = File(args.rootDirectory)

        // Walk through all files in the root directory
        // We reverse the order of the list to present the last photo taken
        mediaList = rootDirectory.listFiles { file ->
            EXTENSION_WHITELIST.contains(file.extension.toUpperCase(Locale.ROOT))
        }?.sortedDescending()?.toMutableList() ?: mutableListOf()

        lastPicture = mediaList[0]

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photo_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callbacks?.setImage(lastPicture)

        view.findViewById<ImageButton>(R.id.send_action).setOnClickListener{
            // disable buttons on send action
            val btn_send = view.findViewById<ImageButton>(R.id.send_action)
            val btn_exit = view.findViewById<ImageButton>(R.id.cancel_action)
            btn_exit.isEnabled = false
            btn_exit.isClickable = false
            btn_send.isEnabled = false
            btn_send.isClickable = false

            callbacks?.sendImage(lastPicture)
        }

        view.findViewById<ImageButton>(R.id.cancel_action).setOnClickListener{
            Navigation.findNavController(requireActivity(), R.id.fragment_container).navigateUp()
        }
    }
}
