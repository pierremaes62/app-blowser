# Image mobile dette

## Inspection du code

### Partie 1
Classes modifié / ajoutés android:
* VolleyFileUploadRaquest
* AddImage
* GalleryFragment
* ajout xml pour interface (fragment_add_image.xml)

Modifications api python:
* modification modèle BDD (File situé dans models.py)
* ajout classe FileSubmitView (dans views.py)

### Partie 2
Classes modifié / ajoutés android:
* Main Activity
* VolleyRateUploadResuest
* PhotoPreview
* modification iterface (fragment_gallery.xml, popup_result.xml

Modifications api python:
* Ajout modèle BDD (Rate situé dans models.py)
* ajout classe FileRateView (dans views.py)

Les parties ajoutés / modifié sont balisés dans le codes par des commentaires contenants "PARTIE 1" ou "PARTIE 2"

## Démarrage de l'application

### Démarrage du serveur

La version de python utilisé pour ces dev est la 3.7

Avant toute chose, il faut commencer par initialiser les BDD du serveur.
Il faut donc lancer la commande:

```sh
python manage.py migrate
```

Il est recommander de lancer un serveur ngrock.
Après avoir installé ngrock, il faut le lancer l'executable et saisir la commande:

```sh
ngrok.exe http 8000
```

Il faudra alors récupérer l'URL ngrock générer et la remplacer dans la partie android du projet, dans la classe MainActivity et AddImage (l'endroit est indiqué par un commentaire).
Il faut aussi ajouter l'URL dans les settings de l'api pythons, dans la table "ALLOWED_HOSTS" se trouvant dans le fichier website/settings.py


Ensuite, il est possible de démarrer le serveur python avec la commande :

```sh
python manage.py runserver
```

### Démarrage de l'application android

L'application se lance via AndroidStudio.

La version du SDK d'android utilisé est la Android 9.0 (version d'api 28)
Après avoir effectué un Graddle install, il devrait être possible d'émuler l'application.


## Les fonctionnalités ajoutés
### Possibilité d'envoyer une image vers le serveur

Cette nouvelle fonctionnalité se situe dans la galerie de photo, accessible via l'icone ronde en bas a droite de l'écran.
On peut choisir ensuite la fonction à evoyer en appuyant sur l'icone d'envoi représenté par un avion en papier.

Une nouvelle fenetre s'ouvre, avec une preview de la photo selmectionné, ainsi que des champs non obligatoires "mail" et "commentaire"

Il est ensuite possible d'envoyer la photo vers le serveur. Celle ci sera stocké en BDD.

### Possibilité de "Upvoter" ou "Downvoter" les résultats envoyés par le serveur

Lors de l'envoi d'une photo vers le serveur pour effectuer une recherche d'image similaire, le serveur renvoie les trois images qui se rapprochent le plus de celle envoyé.
Celle-ci étaient déjà visible à la base via une popup contenant les dites photos.

Désormais, sous chaque résultats, un bouton "V" (Upvote) et "X" (Downvote) sont visibles.
Lors d'une pression sur l'un des deux boutons, on envoie le vote directement vers le serveur, et les deux boutons se désactivent, sans possibilité de revoter pour la meme photo.

Les votes sont stockés dans une nouvelle BDD coté serveur, nommé "image_rate", et contenant le type du vote (rate), l'url de la photo "résultat" envoyé par le serveur (matched_image) et l'id en BDD de la photo que l'on avait envoyé au serveur (sent_image_id, clé étrangère de image_file.id)



